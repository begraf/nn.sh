# nn.sh

There's some information that is required often - but not often enough
to actually remember it.
I use this simple note taking approach based on *vim*, [*fzf.vim*](https://github.com/junegunn/fzf.vim)
and some fancy *grep* implementation, e.g. [*ripgrep*](https://github.com/BurntSushi/ripgrep).

### Setup

It's enough to source `nn.sh` in a `.bashrc` or `.zshrc`.
```bash
source path-to-repo-directory/nn.sh
```

During alias creation, the variable `$NN_NOTE_FILE`
targets the file where notes should be stored,
otherwise the default `$HOME/notes.txt` is used.

```bash
# override the default $HOME/notes.txt
export NN_NOTE_FILE=$HOME/my_notes.txt  
source path-to-repo-directory/nn.sh
```

### Usage

The following commands are used:

* `nn` - fuzzy free search in the notes file
* `nnt` - fuzzy search for topics in the notes file
* `nna` - add an entry at the bottom of the notes file

### Notes file

An entry in the notes file starts with a delimiter line and creation
date followed by the topic, introduced by `%!`.
After that an arbitrary amount of information may be added.

~~~~
%%% ---- [created: 2019-07-27T19:30:45+02:00]
%! This is the topic
Here comes the rest...
.. of the entry.

etc etc..
~~~~

---------------------
bg, 2019
