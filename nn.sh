# Note taking - easy mode
#
# Requirements:
# Vim with fzf plugin
#
# bgraf, 2019

: ${NN_NOTE_FILE:="$HOME/notes.txt"}

alias nna="vim  -c \"execute 'normal! G2o'\" -c \"execute 'normal! zt'\" -c \"execute 'normal! i%%% ---- [created: $(date --iso-8601=s)]
%!  '\" -c \"startinsert\" $NN_NOTE_FILE"
alias nn="vim -c \":BLines\" $NN_NOTE_FILE"
alias nnt="vim -c \":BLines %! \" $NN_NOTE_FILE"

